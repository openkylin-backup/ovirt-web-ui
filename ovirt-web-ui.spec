# Used for rpm-packaging of pre-bundled application with already resolved JS dependencies
%global product oVirt

%global use_rhev %( test -z @RHEV@ && echo 1 || echo 0)
%define debug_package %{nil}

%global source_basename ovirt-web-ui

Name:           ovirt-web-ui
Version:        1.6.0
Release:        1%{?release_suffix}%{?checkout}%{?dist}
Summary:        VM Portal for %{product}
License:        ASL 2.0
URL:            https://github.com/oVirt/ovirt-web-ui
Source0:        https://github.com/oVirt/ovirt-web-ui/archive/%{source_basename}-1.6.0.tar.gz

BuildArch: noarch

# Keep ovirt-engine-{nodejs|nodejs-modules|yarn} at particular version unless tested on higher
BuildRequires: ovirt-engine-nodejs = 8.11.4
BuildRequires: ovirt-engine-yarn = 1.7.0

BuildRequires: ovirt-engine-nodejs-modules >= 1.8.16

%description
This package provides the VM Portal for %{product}.

%prep
# Use the ovirt-engine nodejs installation
# export PATH="%{_datadir}/ovirt-engine-nodejs/bin:${PATH}"

%setup -q -n"%{source_basename}-%{version}"
rpm -qa | grep ovirt-engine-nodejs

echo "=== Workaround: 'yarn check' is recently failing on requiring webpack >= v2. Manually"
echo "=== verified that this is ok for recent code base but may be a real failure in the"
echo "=== future. Please watch carefully for other potential types of errors."
export IGNORE_YARN_CHECK=1
source /usr/share/ovirt-engine-nodejs-modules/setup-env.sh

%build
export PATH="%{_datadir}/ovirt-engine-nodejs/bin:%{_datadir}/ovirt-engine-yarn/bin:${PATH}"
%configure
export PATH="./node_modules/.bin:${PATH}"
make

%install
make install DESTDIR=%{buildroot}

%files
%doc README.md
%license LICENSE
%{_datarootdir}/ovirt-web-ui
%{_datarootdir}/ovirt-engine/ovirt-web-ui.war
%{_sysconfdir}/ovirt-engine/engine.conf.d/50-ovirt-web-ui.conf
%{_sysconfdir}/ovirt-web-ui/branding/00-ovirt.brand

%changelog
* Thu Mar 5 2020 di.wang <di.wang@cs2c.com.cn> - 1.6.0-1
- Package Initialization
